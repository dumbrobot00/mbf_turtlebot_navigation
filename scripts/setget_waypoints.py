import rospy
import smach
import threading
from geometry_msgs.msg import PoseStamped

class SetWaypoints(smach.State):

    def __init__(self):
        smach.State.__init__(self, outcomes=['waypoints_set', 'preempted', 'failed'], input_keys=['waypoints_in'], output_keys=['target_pose', 'waypoints_out'])
        self.target_pose = PoseStamped()
       # self.waypoints = []
        self.subscriber = None

    def execute(self, user_data):

        waypoints = user_data.waypoints_in

        waypoints[:] = [] #clear the waypoints list

#    (1.029064, 0.988091, 0.000000) (0.000000, 0.000000, 0.014346, 0.999897)

        temp_waypoint = PoseStamped()
        temp_waypoint.header.frame_id = 'map'
        temp_waypoint.pose.position.x = 1 
        temp_waypoint.pose.position.y = 1 
        temp_waypoint.pose.position.z = 0
        temp_waypoint.pose.orientation.x = 0
        temp_waypoint.pose.orientation.y = 0
        temp_waypoint.pose.orientation.z = 0
        temp_waypoint.pose.orientation.w = 1 

        
        waypoints.append([temp_waypoint.pose.position.x,
                          temp_waypoint.pose.position.y,
                          temp_waypoint.pose.position.z,
                          temp_waypoint.pose.orientation.x,
                          temp_waypoint.pose.orientation.y,
                          temp_waypoint.pose.orientation.z,
                          temp_waypoint.pose.orientation.w]) 
        
#        waypoints.append(temp_waypoint)


        print(waypoints)
        print("=========")

#    (-0.998011, 1.018338, 0.000000) (0.000000, 0.000000, -0.706891, 0.707322)

        temp_waypoint.pose.position.x = -1 
        temp_waypoint.pose.position.y = 1 
        temp_waypoint.pose.position.z = 0
        temp_waypoint.pose.orientation.x = 0
        temp_waypoint.pose.orientation.y = 0
        temp_waypoint.pose.orientation.z = -0.707
        temp_waypoint.pose.orientation.w = 0.707
       
        waypoints.append([temp_waypoint.pose.position.x,
                          temp_waypoint.pose.position.y,
                          temp_waypoint.pose.position.z,
                          temp_waypoint.pose.orientation.x,
                          temp_waypoint.pose.orientation.y,
                          temp_waypoint.pose.orientation.z,
                          temp_waypoint.pose.orientation.w]) 

        
#        waypoints.append(temp_waypoint)

        print(waypoints)
        print("=========")




        temp_waypoint.pose.position.x = 0
        temp_waypoint.pose.position.y = -1 
        temp_waypoint.pose.position.z = 0
#        temp_waypoint.pose.orientation.x = 0
#        temp_waypoint.pose.orientation.y = 0
#        temp_waypoint.pose.orientation.z = 0
#        temp_waypoint.pose.orientation.w = 0
        
        waypoints.append([temp_waypoint.pose.position.x,
                          temp_waypoint.pose.position.y,
                          temp_waypoint.pose.position.z,
                          temp_waypoint.pose.orientation.x,
                          temp_waypoint.pose.orientation.y,
                          temp_waypoint.pose.orientation.z,
                          temp_waypoint.pose.orientation.w]) 
#        waypoints.append(temp_waypoint)

        waypoints_count = len(waypoints)


        rospy.loginfo('No. waypoints = %d', waypoints_count)
        for i in range(0, waypoints_count):
            rospy.loginfo("Wpt[%d] : (%f, %f, %f)", 
                           i, 
                           waypoints[i][0],
                           waypoints[i][1],
                           waypoints[i][2])


        print(waypoints)

        if self.preempt_requested() or rospy.is_shutdown():
            self.service_preempt()
            return 'preempted'


        if len(waypoints) == 0:
            return 'failed'

        user_data.waypoints_out = waypoints
        return 'waypoints_set'



class GetWaypoint(smach.State):

    def __init__(self):
        smach.State.__init__(self, outcomes=['waypoint_got', 'preempted', 'emptied_waypoint_queue'], input_keys=['waypoints_in'], output_keys=['target_pose', 'waypoints_out'])
        self.target_pose = PoseStamped()
        self.subscriber = None

    def execute(self, user_data):

        waypoints = user_data.waypoints_in

        # Break if preempted
        if waypoints == []:
            rospy.loginfo('The waypoint queue has been reset.')
            return 'emptied_waypoint_queue' 

        temp_waypoint = waypoints.pop(0) #should be PoseStamped
        
        current_waypoint = PoseStamped()
        current_waypoint.header.frame_id = 'map'
        current_waypoint.pose.position.x = temp_waypoint[0]
        current_waypoint.pose.position.y = temp_waypoint[1]
        current_waypoint.pose.position.z = temp_waypoint[2] 
        current_waypoint.pose.orientation.x = temp_waypoint[3]
        current_waypoint.pose.orientation.y = temp_waypoint[4]
        current_waypoint.pose.orientation.z = temp_waypoint[5]
        current_waypoint.pose.orientation.w = temp_waypoint[6]

       

        rospy.loginfo("Reading the popped waypoint: (%s, %s, %s)", 
                       current_waypoint.pose.position.x,
                       current_waypoint.pose.position.y,
                       current_waypoint.pose.position.z)


        if self.preempt_requested() or rospy.is_shutdown():
            self.service_preempt()
            return 'preempted'

        user_data.target_pose = current_waypoint
        user_data.waypoints_out = waypoints
        return 'waypoint_got'


    def request_preempt(self):
        smach.State.request_preempt(self)
#        self.signal.set()

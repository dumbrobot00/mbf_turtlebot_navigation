# **turtlebot_gazebo_mbf**

An example of using mbf on a simulated (GazeboSim) turtlebot 2 (Kobuki).

This example was modified with reference from 

*  https://github.com/uos/ceres_robot (ceres_navigation) 

*  https://github.com/turtlebot/turtlebot_apps (turtlebot_navigation).

-------------------------------------------------------------------------------------------

## Setup Instruction

### Creat an empty workspace



> $ mkdir mbf_turtlebot_gazeboi_ws 

> $ cd mbf_turtlebot_gazebo_ws 

> mbf_turtlebot_gazebo_ws$ mkdir build

> mbf_turtlebot_gazebo_ws$ mkdir devel

> mbf_turtlebot_gazebo_ws$ mkdir src

> mbf_turtlebot_gazebo_ws$ catkin_make

> mbf_turtlebot_gazebo_ws$ cd src

> mbf_turtlebot_gazebo_ws/src$ git clone https://gitlab.com/dumbrobot00/mbf_turtlebot_navigation.git


### Install or clone move_base_flex
If pre-built ros package is preferred, use apt tool to install.
> $ sudo apt install ros-kinetic-move-base-flex

else clone from git repository and rebuild it.
> mbf_turtlebot_gazebo_ws/src$ git clone https://github.com/magazino/move_base_flex.git

### Verify if turtlebot_gazebo is already installed
> $ roslaunch turtlebot_gazebo turtlebot_world.launch

A gazebo simulation of turtlebot 2 (kobuki) should appear.
If nothing appears and an error message states no such package, that might mean turtlebot_gazebo has not been installed.
You can install pre-built ros package or clone and build the packages.

If pre-built ros package is preferred, use apt tool to install.
> $ sudo apt install ros-kinetic-turtlebot

> $ sudo apt install ros-kinetic-turtlebot-apps

> $ sudo apt install ros-kinetic-turtlebot-simulator or sudo apt install ros-kinetic-turtlebot-gazebo 

else clone from git repository and rebuild it.
> $ mbf_turtlebot_gazebo_ws/src$ git clone https://github.com/turtlebot/turtlebot.git

> $ mbf_turtlebot_gazebo_ws/src$ git clone https://github.com/turtlebot/turtlebot_apps.git

> $ mbf_turtlebot_gazebo_ws/src$ git clone https://github.com/turtlebot/turtlebot_simulator.git
  

### Install ROS Search-based Latice Library wrapper package
If pre-built ros package is preferred, use apt tool to install.
> $ sudo apt install ros-kinetic-navigation-experimental 

else clone from git repository and rebuild it.
> mbf_turtlebot_gazebo_ws/src$ git clone https://github.com/ros-planning/navigation_experimental.git 

 
### Install SBPL
Follow the instructions in README.txt on https://github.com/sbpl/sbpl
Building and Installing SBPL from source works well.
> mbf_turtlebot_gazebo_ws/src$ git clone https://github.com/sbpl/sbpl.git  

> mbf_turtlebot_gazebo_ws/src$ cd sbpl

> mbf_turtlebot_gazebo_ws/src/sbpl$ mkdir build

> mbf_turtlebot_gazebo_ws/src/sbpl$ cd build   

> mbf_turtlebot_gazebo_ws/src/sbpl/build$ cmake .. 

> mbf_turtlebot_gazebo_ws/src/sbpl/build$ make

> mbf_turtlebot_gazebo_ws/src/sbpl/build$ sudo make install

Build the workspace using catkin_make
> mbf_turtlebot_gazebo_ws/src$ cd .. 
> mbf_turtlebot_gazebo_ws$ catkin_make
 


-------------------------------------------------------------------------------------------

## Run MBF using dwa or eband local planner
Open 3 terminals at the current workspace, i.e. mbf_turtlebot_gazebo_ws
Remember to source the setup bash file for each terminal
> mbf_turtlebot_gazebo_ws$ source devel/setup.bash

Start turtlebot_gazebo simulation from the first terminal.
> mbf_turtlebot_gazebo_ws$ roslaunch turtlebot_gazebo turtlebot_world.launch

A Gazebo simulator with a turtlebot in a simple environment will appear.


Start navigation with move_base_flex from the second terminal
> mbf_turtlebot_gazebo_ws$ roslaunch mbf_turtlebot_navigation mbf_amcl_demo.launch



If move_base_flex started successfully, INFO messages should be similar below.
```[ INFO] [1570368904.572698918]: Initializing nodelet with 4 worker threads.

[ INFO] [1570368904.982923104, 27.850000000]: No matching device found.... waiting for devices. Reason: std::__cxx11::string openni2_wrapper::OpenNI2Driver::resolveDeviceURI(const string&) @ /tmp/binarydeb/ros-kinetic-openni2-camera-0.4.2/src/openni2_driver.cpp @ 737 : Invalid device number 1, there are 0 devices connected.

[ INFO] [1570368905.234781861, 28.090000000]: Using plugin "obstacle_layer"

[ INFO] [1570368905.247372019, 28.110000000]:     Subscribed to Topics: scan bump

[ INFO] [1570368904.572698918]: Initializing nodelet with 4 worker threads.

[ INFO] [1570368904.982923104, 27.850000000]: No matching device found.... waiting for devices. Reason: std::__cxx11::string openni2_wrapper::OpenNI2Driver::resolveDeviceURI(const string&) @ /tmp/binarydeb/ros-kinetic-openni2-camera-0.4.2/src/openni2_driver.cpp @ 737 : Invalid device number 1, there are 0 devices connected.

[ INFO] [1570368905.234781861, 28.090000000]: Using plugin "obstacle_layer"

[ INFO] [1570368905.247372019, 28.110000000]:     Subscribed to Topics: scan bump

[INFO] [1570368905.386575, 28.250000]: State machine starting in initial state 'WAIT_FOR_GOAL' with userdata: 
	['recovery_behavior_index']

[INFO] [1570368905.386825, 28.250000]: Waiting for a goal...

[ INFO] [1570368905.423579483, 28.290000000]: Using plugin "inflation_layer"

[ INFO] [1570368906.473478063, 29.350000000]: Using plugin "static_layer"

[ INFO] [1570368906.480241102, 29.350000000]: Requesting the map...

[ INFO] [1570368906.778888265, 29.660000000]: Resizing costmap to 231 X 194 at 0.050000 m/pix

[ INFO] [1570368906.877345385, 29.760000000]: Received a 231 X 194 map at 0.050000 m/pix

[ INFO] [1570368906.882879843, 29.760000000]: Using plugin "obstacle_layer"

[ INFO] [1570368906.885034016, 29.760000000]:     Subscribed to Topics: scan bump

[ INFO] [1570368906.943013092, 29.820000000]: Using plugin "inflation_layer"

[ INFO] [1570368907.069985217, 29.950000000]: The plugin with the type "global_planner/GlobalPlanner" has been loaded successfully under the name "GlobalPlanner".

[ INFO] [1570368907.164428081, 30.040000000]: The plugin with the type "eband_local_planner/EBandPlannerROS" has been loaded successfully under the name "eband".

[ INFO] [1570368907.173111934, 30.050000000]: Sim period is set to 0.05

[ INFO] [1570368907.270144587, 30.150000000]: The plugin with the type "dwa_local_planner/DWAPlannerROS" has been loaded successfully under the name "dwa".

[ INFO] [1570368907.274423709, 30.150000000]: The plugin with the type "rotate_recovery/RotateRecovery" has been loaded successfully under the name "rotate_recovery".

[ INFO] [1570368907.277006019, 30.150000000]: Recovery behavior will clear layer obstacles

[ INFO] [1570368907.277113200, 30.150000000]: The plugin with the type "clear_costmap_recovery/ClearCostmapRecovery" has been loaded successfully under the name "clear_costmap".

[ INFO] [1570368907.361043399, 30.240000000]: odom received!

```




Run RVIZ from the third terminal
> $ roslaunch turtlebot_rviz_launchers view_navigation.launch

An rviz window with the correct map and the turtlebot model will appear.


### Determine which local planner was set
Check `mbf_turtlebot_navigation/script/plan_exec_sm.py` to see if dwa or eband local planner was set.
at *line 44*
```        # Add a planner statemachine for every planner specified
        with self:
            for index, planner in enumerate(self._planners):
                planner_sm = PlannerStateMachine(index, planner, 'eband')
                smach.Concurrence.add(planner.upper(), planner_sm)
```


If we are running dwa local planner, choose rviz config file `mbf_turtlebot_navigation/launch/mbf_turtlebot_dwa.rviz`

If we are rurring eband local planner, choose rviz config file `mbf_turtlebot_navigation/launch/mbf_turtlebot_eband.rviz`


### Test navigation
Set 2D nav goals in RVIZ and watch the robot move.

Also check the second terminal (which the move_base_flex is running) for path planning messages similar below.
```[INFO] [1570368983.110252, 105.960000]: Received goal pose: (1.11110961437, 0.682219445705, 0.0)
[INFO] [1570368983.110777, 105.960000]: State machine transitioning 'WAIT_FOR_GOAL':'received_goal'-->'PLAN_EXEC'
[INFO] [1570368983.111600, 105.960000]: Concurrence starting with userdata: 
	['target_pose']
[INFO] [1570368983.112784, 105.960000]: State machine starting in initial state 'GLOBALPLANNER' with userdata: 
	['target_pose']
[ INFO] [1570368983.113900037, 105.970000000]: Start action "get_path" using planner "GlobalPlanner" of type "global_planner/GlobalPlanner"
[INFO] [1570368983.121026, 105.970000]: State machine transitioning 'GLOBALPLANNER':'succeeded'-->'GLOBALPLANNER_EXEC'
[ INFO] [1570368983.136333112, 105.990000000]: Start action "exe_path" using controller "eband" of type "eband_local_planner/EBandPlannerROS"
[ INFO] [1570368985.987143283, 108.830000000]: No matching device found.... waiting for devices. Reason: std::__cxx11::string openni2_wrapper::OpenNI2Driver::resolveDeviceURI(const string&) @ /tmp/binarydeb/ros-kinetic-openni2-camera-0.4.2/src/openni2_driver.cpp @ 737 : Invalid device number 1, there are 0 devices connected.
[ INFO] [1570368986.709829279, 109.560000000]: TrajectoryController: Goal reached with distance 0.03, 0.00 (od = -0.20); sending zero velocity
[INFO] [1570368986.761008, 109.610000]: State machine terminating 'GLOBALPLANNER_EXEC':'succeeded':'succeeded'
[INFO] [1570368986.761674, 109.610000]: Concurrent state 'GLOBALPLANNER' returned outcome 'succeeded' on termination.
[INFO] [1570368986.775219, 109.620000]: Concurrent Outcomes: {'GLOBALPLANNER': 'succeeded'}
[INFO] [1570368986.775626, 109.620000]: State machine transitioning 'PLAN_EXEC':'succeeded'-->'WAIT_FOR_GOAL'
[INFO] [1570368986.787347, 109.630000]: Waiting for a goal...
```
This means the global/local planner and move_base_flex SMACH was working properly.


### Testing videos
*  MBF DWA: https://youtu.be/KpgEi1n2D74
*  MBF EBAND: https://youtu.be/TkXGZ_aS4C4


-------------------------------------------------------------------------------------------

## Run waypoints test using MBF and SMACH

Close all previous terminals first.
Open 3 terminals at the current workspace, i.e. mbf_turtlebot_gazebo_ws
Remember to source the setup bash file for each terminal
> mbf_turtlebot_gazebo_ws$ source devel/setup.bash

Start turtlebot_gazebo simulation from the first terminal.
> mbf_turtlebot_gazebo_ws$ roslaunch turtlebot_gazebo turtlebot_world.launch

A Gazebo simulator with a turtlebot in a simple environment will appear.


Start waypoint test with move_base_flex from the second terminal
> mbf_turtlebot_gazebo_ws$ roslaunch mbf_turtlebot_navigation mbf_waypoint_test.launch

If waypoint test started successfully, the turtlebot in the Gazebo simulation will move around 3 predefined waypoints repeatedly.
The console INFO messages should be similar below.

```
process[navigation_velocity_smoother-17]: started with pid [9461]
process[kobuki_safety_controller-18]: started with pid [9481]
process[move_base_flex-19]: started with pid [9491]
[ INFO] [1571819064.834690252]: Initializing nodelet with 4 worker threads.
process[mbf_state_machine-20]: started with pid [9499]
[ INFO] [1571819065.619655605, 1051.620000000]: Using plugin "obstacle_layer"
[ INFO] [1571819065.644301706, 1051.640000000]: No devices connected.... waiting for devices to be connected
[ INFO] [1571819065.649075348, 1051.650000000]:     Subscribed to Topics: scan bump
[INFO] [1571819065.678663, 1051.680000]: State machine starting in initial state 'SET_WAYPOINTS' with userdata: 
	['planner', 'recovery_behavior', 'waypoints', 'previous_state', 'goal_position', 'controller', 'error_status', 'recovery_behaviors', 'error', 'act_pos', 'path', 'clear_costmap_flag']
[[1, 1, 0, 0, 0, 0, 1]]
=========
[[1, 1, 0, 0, 0, 0, 1], [-1, 1, 0, 0, 0, -0.707, 0.707]]
=========
[INFO] [1571819065.678964, 1051.680000]: No. waypoints = 3
[INFO] [1571819065.679086, 1051.680000]: Wpt[0] : (1.000000, 1.000000, 0.000000)
[INFO] [1571819065.679193, 1051.680000]: Wpt[1] : (-1.000000, 1.000000, 0.000000)
[INFO] [1571819065.679295, 1051.680000]: Wpt[2] : (0.000000, -1.000000, 0.000000)
[[1, 1, 0, 0, 0, 0, 1], [-1, 1, 0, 0, 0, -0.707, 0.707], [0, -1, 0, 0, 0, -0.707, 0.707]]
[INFO] [1571819065.679448, 1051.680000]: State machine transitioning 'SET_WAYPOINTS':'waypoints_set'-->'GET_WAYPOINT'
[INFO] [1571819065.679966, 1051.680000]: Reading the popped waypoint: (1, 1, 0)
[INFO] [1571819065.680155, 1051.680000]: State machine transitioning 'GET_WAYPOINT':'waypoint_got'-->'MOVE_BASE'
[WARN] [1571819065.681305, 1051.680000]: Still waiting for action server 'move_base_flex/move_base' to start... is it running?
[ INFO] [1571819065.838249960, 1051.840000000]: Using plugin "inflation_layer"
[ INFO] [1571819067.439355089, 1053.460000000]: Using plugin "static_layer"
[ INFO] [1571819067.443341798, 1053.460000000]: Requesting the map...
[ INFO] [1571819067.746044054, 1053.770000000]: Resizing costmap to 231 X 194 at 0.050000 m/pix
[ INFO] [1571819067.845527760, 1053.870000000]: Received a 231 X 194 map at 0.050000 m/pix
[ INFO] [1571819067.851201222, 1053.870000000]: Using plugin "obstacle_layer"
[ INFO] [1571819067.854859701, 1053.870000000]:     Subscribed to Topics: scan bump
[ INFO] [1571819067.913480309, 1053.930000000]: Using plugin "inflation_layer"
[ INFO] [1571819068.034762961, 1054.050000000]: The plugin with the type "global_planner/GlobalPlanner" has been loaded successfully under the name "GlobalPlanner".
[ INFO] [1571819068.124942438, 1054.140000000]: The plugin with the type "eband_local_planner/EBandPlannerROS" has been loaded successfully under the name "eband".
[ INFO] [1571819068.133231596, 1054.150000000]: Sim period is set to 0.05
[ INFO] [1571819068.230879897, 1054.250000000]: The plugin with the type "dwa_local_planner/DWAPlannerROS" has been loaded successfully under the name "dwa".
[ INFO] [1571819068.235714326, 1054.250000000]: The plugin with the type "rotate_recovery/RotateRecovery" has been loaded successfully under the name "rotate_recovery".
[ INFO] [1571819068.237755463, 1054.250000000]: Recovery behavior will clear layer obstacles
[ INFO] [1571819068.237952126, 1054.250000000]: The plugin with the type "clear_costmap_recovery/ClearCostmapRecovery" has been loaded successfully under the name "clear_costmap".
[ INFO] [1571819068.298547367, 1054.310000000]: odom received!
[INFO] [1571819068.453628, 1054.470000]: Connected to action server 'move_base_flex/move_base'.
[ INFO] [1571819068.454404775, 1054.470000000]: Start action "move_base"
[ INFO] [1571819068.455029347, 1054.470000000]: Start action "get_path" using planner "GlobalPlanner" of type "global_planner/GlobalPlanner"
[ INFO] [1571819068.459922729, 1054.470000000]: Start action "exe_path" using controller "dwa" of type "dwa_local_planner/DWAPlannerROS"
[ INFO] [1571819068.469016857, 1054.490000000]: Got new plan
[ INFO] [1571819068.644445347, 1054.660000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819068.854797904, 1054.870000000]: Calculation needs too much time to stay in the moving frequency! (0.067972 > 0.050000)
[ WARN] [1571819069.569148973, 1055.590000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2300 seconds
[ WARN] [1571819069.819476113, 1055.840000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2800 seconds
[ WARN] [1571819070.052988095, 1056.070000000]: Calculation needs too much time to stay in the moving frequency! (0.226049 > 0.050000)
[ WARN] [1571819070.590843734, 1056.610000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2500 seconds
[ WARN] [1571819070.878331401, 1056.890000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3300 seconds
[ WARN] [1571819071.163728984, 1057.170000000]: Calculation needs too much time to stay in the moving frequency! (0.278090 > 0.050000)
[ WARN] [1571819071.165184582, 1057.180000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4200 seconds
[ WARN] [1571819071.461335534, 1057.470000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2900 seconds
[ INFO] [1571819071.644597224, 1057.660000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819071.752408965, 1057.760000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3800 seconds
[ WARN] [1571819072.325853662, 1058.340000000]: Calculation needs too much time to stay in the moving frequency! (0.280436 > 0.050000)
[ WARN] [1571819072.326542615, 1058.340000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.7600 seconds
[ WARN] [1571819072.619553762, 1058.630000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2900 seconds
[ WARN] [1571819072.919741921, 1058.930000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3900 seconds
[ WARN] [1571819073.207615854, 1059.220000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4800 seconds
[ WARN] [1571819073.476084668, 1059.480000000]: Calculation needs too much time to stay in the moving frequency! (0.267954 > 0.050000)
[ WARN] [1571819073.751044734, 1059.750000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.5300 seconds
[ WARN] [1571819073.992026371, 1059.990000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2400 seconds
[ WARN] [1571819074.255338315, 1060.260000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3100 seconds
[ WARN] [1571819074.523214253, 1060.530000000]: Calculation needs too much time to stay in the moving frequency! (0.267241 > 0.050000)
[ WARN] [1571819074.524300428, 1060.530000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3800 seconds
[ INFO] [1571819074.644748520, 1060.650000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819074.743081316, 1060.750000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4000 seconds
[ WARN] [1571819074.920328730, 1060.930000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3800 seconds
[ WARN] [1571819075.062846840, 1061.070000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3200 seconds
[ WARN] [1571819075.184410640, 1061.190000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2400 seconds
[ WARN] [1571819075.515340452, 1061.530000000]: Calculation needs too much time to stay in the moving frequency! (0.107683 > 0.050000)
[ WARN] [1571819076.604459566, 1062.620000000]: Calculation needs too much time to stay in the moving frequency! (0.124988 > 0.050000)
[ INFO] [1571819077.645172393, 1063.650000000]: No devices connected.... waiting for devices to be connected
[ INFO] [1571819078.650240197, 1064.660000000]: Goal reached
[ INFO] [1571819078.650738633, 1064.660000000]: Action "move_base" succeeded!
[INFO] [1571819078.652277, 1064.670000]: State machine transitioning 'MOVE_BASE':'success'-->'GET_WAYPOINT'
[INFO] [1571819078.653303, 1064.670000]: Reading the popped waypoint: (-1, 1, 0)
[INFO] [1571819078.653653, 1064.670000]: State machine transitioning 'GET_WAYPOINT':'waypoint_got'-->'MOVE_BASE'
[ INFO] [1571819078.655091009, 1064.670000000]: Start action "move_base"
[ INFO] [1571819078.655450818, 1064.670000000]: Start action "get_path" using planner "GlobalPlanner" of type "global_planner/GlobalPlanner"
[ INFO] [1571819078.658928843, 1064.670000000]: Start action "exe_path" using controller "dwa" of type "dwa_local_planner/DWAPlannerROS"
[ INFO] [1571819078.666926368, 1064.680000000]: Got new plan
[ WARN] [1571819078.830914941, 1064.850000000]: DWA planner failed to produce path.
[ WARN] [1571819078.831195578, 1064.850000000]: No velocity command received from controller! Controller failed
[ WARN] [1571819078.882685974, 1064.900000000]: DWA planner failed to produce path.
[ WARN] [1571819078.934153244, 1064.950000000]: DWA planner failed to produce path.
[ WARN] [1571819078.984401294, 1065.000000000]: DWA planner failed to produce path.
[ WARN] [1571819079.402266140, 1065.420000000]: Calculation needs too much time to stay in the moving frequency! (0.053538 > 0.050000)
[ WARN] [1571819080.428059118, 1066.440000000]: Calculation needs too much time to stay in the moving frequency! (0.204964 > 0.050000)
[ INFO] [1571819080.645315642, 1066.660000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819081.479534436, 1067.490000000]: Calculation needs too much time to stay in the moving frequency! (0.280124 > 0.050000)
[ WARN] [1571819081.763925193, 1067.770000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2200 seconds
[ WARN] [1571819082.046277829, 1068.060000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3100 seconds
[ WARN] [1571819082.356331375, 1068.360000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4100 seconds
[ WARN] [1571819082.646996136, 1068.650000000]: Calculation needs too much time to stay in the moving frequency! (0.283955 > 0.050000)
[ WARN] [1571819082.648483786, 1068.660000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3000 seconds
[ WARN] [1571819082.945582785, 1068.950000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3900 seconds
[ WARN] [1571819083.237027430, 1069.240000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4800 seconds
[ INFO] [1571819083.645460511, 1069.650000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819083.729644487, 1069.730000000]: Calculation needs too much time to stay in the moving frequency! (0.127274 > 0.050000)
[ WARN] [1571819084.803057854, 1070.810000000]: Calculation needs too much time to stay in the moving frequency! (0.146273 > 0.050000)
[ WARN] [1571819085.874564135, 1071.880000000]: Calculation needs too much time to stay in the moving frequency! (0.126287 > 0.050000)
[ INFO] [1571819086.645605275, 1072.650000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819086.936351216, 1072.940000000]: Calculation needs too much time to stay in the moving frequency! (0.138038 > 0.050000)
[ WARN] [1571819088.054420478, 1074.060000000]: Calculation needs too much time to stay in the moving frequency! (0.140406 > 0.050000)
[ WARN] [1571819089.073166619, 1075.080000000]: Calculation needs too much time to stay in the moving frequency! (0.121408 > 0.050000)
[ INFO] [1571819089.645743942, 1075.650000000]: No devices connected.... waiting for devices to be connected
[ INFO] [1571819092.074482374, 1078.080000000]: Goal reached
[ INFO] [1571819092.074713422, 1078.080000000]: Action "move_base" succeeded!
[INFO] [1571819092.075369, 1078.080000]: State machine transitioning 'MOVE_BASE':'success'-->'GET_WAYPOINT'
[INFO] [1571819092.076184, 1078.080000]: Reading the popped waypoint: (0, -1, 0)
[INFO] [1571819092.076477, 1078.080000]: State machine transitioning 'GET_WAYPOINT':'waypoint_got'-->'MOVE_BASE'
[ INFO] [1571819092.077843788, 1078.080000000]: Start action "move_base"
[ INFO] [1571819092.078203036, 1078.080000000]: Start action "get_path" using planner "GlobalPlanner" of type "global_planner/GlobalPlanner"
[ INFO] [1571819092.084820916, 1078.090000000]: Start action "exe_path" using controller "dwa" of type "dwa_local_planner/DWAPlannerROS"
[ INFO] [1571819092.092533973, 1078.100000000]: Got new plan
[ WARN] [1571819092.253139338, 1078.260000000]: DWA planner failed to produce path.
[ WARN] [1571819092.253299902, 1078.260000000]: No velocity command received from controller! Controller failed
[ WARN] [1571819092.306278902, 1078.310000000]: DWA planner failed to produce path.
[ INFO] [1571819092.645881520, 1078.650000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819092.677759835, 1078.680000000]: Calculation needs too much time to stay in the moving frequency! (0.059070 > 0.050000)
[ WARN] [1571819093.804527262, 1079.810000000]: Calculation needs too much time to stay in the moving frequency! (0.276049 > 0.050000)
[ WARN] [1571819093.807505900, 1079.810000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.5700 seconds
[ WARN] [1571819094.100360180, 1080.100000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2900 seconds
[ WARN] [1571819094.674093578, 1080.680000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2700 seconds
[ WARN] [1571819094.956398832, 1080.960000000]: Calculation needs too much time to stay in the moving frequency! (0.281771 > 0.050000)
[ WARN] [1571819094.956892583, 1080.960000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3500 seconds
[ WARN] [1571819095.238820834, 1081.240000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4300 seconds
[ WARN] [1571819095.529501738, 1081.530000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2900 seconds
[ INFO] [1571819095.646024064, 1081.650000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819095.811593094, 1081.810000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3700 seconds
[ WARN] [1571819096.096282196, 1082.100000000]: Calculation needs too much time to stay in the moving frequency! (0.284696 > 0.050000)
[ WARN] [1571819096.372881066, 1082.370000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.7300 seconds
[ WARN] [1571819096.660085365, 1082.660000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2900 seconds
[ WARN] [1571819096.935408101, 1082.930000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3600 seconds
[ WARN] [1571819097.191563344, 1083.190000000]: Calculation needs too much time to stay in the moving frequency! (0.256023 > 0.050000)
[ WARN] [1571819097.192592020, 1083.190000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4200 seconds
[ WARN] [1571819097.637239831, 1083.640000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4500 seconds
[ WARN] [1571819097.866523644, 1083.870000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2300 seconds
[ WARN] [1571819098.082173212, 1084.080000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2400 seconds
[ WARN] [1571819098.322803695, 1084.320000000]: Calculation needs too much time to stay in the moving frequency! (0.240753 > 0.050000)
[ WARN] [1571819098.324003603, 1084.320000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2800 seconds
[ WARN] [1571819098.585598439, 1084.590000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3500 seconds
[ INFO] [1571819098.646179789, 1084.640000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819098.786204694, 1084.780000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3400 seconds
[ WARN] [1571819098.999841040, 1085.000000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.3600 seconds
[ WARN] [1571819099.319708503, 1085.310000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.4700 seconds
[ WARN] [1571819099.441097566, 1085.440000000]: Calculation needs too much time to stay in the moving frequency! (0.121002 > 0.050000)
[ WARN] [1571819100.558386439, 1086.550000000]: Calculation needs too much time to stay in the moving frequency! (0.134434 > 0.050000)
[ WARN] [1571819101.638247644, 1087.630000000]: Calculation needs too much time to stay in the moving frequency! (0.156636 > 0.050000)
[ INFO] [1571819101.646327720, 1087.630000000]: No devices connected.... waiting for devices to be connected
[ WARN] [1571819102.565470340, 1088.560000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2500 seconds
[ WARN] [1571819102.765576853, 1088.760000000]: needs too much time to stay in the moving frequency! (0.199046 > 0.050000)
[ WARN] [1571819102.767490792, 1088.770000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2600 seconds
[ WARN] [1571819102.951386548, 1088.950000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2400 seconds
[ WARN] [1571819103.134709100, 1089.130000000]: Map update loop missed its desired rate of 5.0000Hz... the loop actually took 0.2200 seconds
[ WARN] [1571819103.783715126, 1089.780000000]: Calculation needs too much time to stay in the moving frequency! (0.152023 > 0.050000)
[ INFO] [1571819104.411979212, 1090.410000000]: Goal reached
[ INFO] [1571819104.412199953, 1090.410000000]: Action "move_base" succeeded!
[INFO] [1571819104.412922, 1090.410000]: State machine transitioning 'MOVE_BASE':'success'-->'GET_WAYPOINT'
[INFO] [1571819104.413632, 1090.410000]: The waypoint queue has been reset.
[INFO] [1571819104.413908, 1090.410000]: State machine transitioning 'GET_WAYPOINT':'emptied_waypoint_queue'-->'SET_WAYPOINTS'

```

### Testing Video
* SMACH waypoints following : https://youtu.be/FqyREHSiP2A

-------------------------------------------------------------------------------------------



-------------------------------------------------------------------------------------------

## Notes
`mbf_turtlebot_navigation/config` hosts all the yaml parameter files for *controller(local planner)*, *planner(global planner)*, *local and global costmap*, and *recovery_behavior*.
Which controller and planner plugins to load is defined by `controller.yaml` and `planner.yaml`.

`mbf_turtlebot_navigation/scripts` hosts all the state machine scripts.
Default scripts for this example is `mbf_state_machine.py`. This script import(calls) from `wait_for_goal.py`, `plan_exec_sm.py`, and `smach_pollyfill.py`.
Note `mbf_move_base_action.py` will also work.
`mbf_simple_sm.py` did **NOT** work. Do **NOT** use it.

`mbf_turtlebot_navigation/launch/mbf_amcl_demo.launch` is the top level launch file, which launches *turtlebot gazebo model*, *map server*, *AMCL*, and *move_base_flex actual launch file*.
This launch file calls `mbf_turtlebot_navigation/launch/includes/move_base_flex.launch.xml` , which acutally *setup the parameters* and *launches move_base_flex node*.
